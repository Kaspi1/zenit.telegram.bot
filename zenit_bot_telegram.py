# -*- coding: utf-8 -*- 
# encoding: utf-8

import datetime
import time
import urllib2
import locale
import logging
import threading
import random

import telepot
import requests
import requests_cache
import ConfigParser

import get_twits


configParser = ConfigParser.RawConfigParser()
configFilePath = r'config.ini'
configParser.read(configFilePath)

telegram_api = configParser.get('telegram', 'telegram_api_key')
chat_id = configParser.get('telegram', 'chat_id')

locale.setlocale(locale.LC_TIME, 'ru_RU.utf8')
logging.basicConfig(filename='run.log',level=logging.WARNING, format='%(levelname)s:%(message)s')
telebot = telepot.Bot(telegram_api)
requests_cache.install_cache('matches_cache',  expire_after=72000)

ar_years = [u'год', u'года', u'лет']
ar_days = [u'день', u'дня', u'дней']
ar_hours = [u'час', u'часа', u'часов']
ar_minutes = [u'минута', u'минуты', u'минут']

def sklon(numeral, words):
    if  ((numeral >= 11) and (numeral <= 14) ):
        return (str(numeral) +" "+  words[2])
    if numeral % 10 == 1: 
        return (str(numeral) +" "+ words[0])
    if ( (numeral % 10 == 2) or (numeral % 10 == 3) or  (numeral % 10 == 4)):
        return (str(numeral) +" "+ words[1] )
    return (str(numeral) +" "+ words[2])


def getMatches_sru():

	headers = {
	           "Host":"www.sports.ru",
	           "Connection": "Keep-Alive",
			   }
	r = requests.get("http://www.sports.ru/stat/export/iphone/team_full_calendar.json?tag=1044514", headers=headers)
	return r.json()


def parseMatches(**kwargs):

	data = getMatches_sru()  

	for game in data:
		club1 = game['command1']['name']
		club2 = game['command2']['name']
		if (game['status_id'] == 1):
			a = [club1 , club2, game['time']]
			return a
	


def why_god_why():
	locale.setlocale(locale.LC_TIME, 'ru_RU.utf8')
	arena_start = datetime.datetime.fromtimestamp(1180699200)
	now = datetime.datetime.now()
	intro  = [u'Кстати,', u'Да,', u'А', u'Вы наверное забыли,', u'Не расстраивайтесь, но,']

	r = random.randrange(len(intro))
	delta = now - arena_start
	t  = u'%s стадион строят уже %s и %s' % (intro[r], sklon(int(delta.days /  365), ar_years), sklon(int(delta.days %  365), ar_days))
	return t


def send_linup():
	locale.setlocale(locale.LC_TIME, 'en_US.utf8')
	t = get_twits.get_twit(club1=club1,club2=club2)
	twit_time = datetime.datetime.fromtimestamp(time.mktime(time.strptime(t[1],"%a %b %d %H:%M:%S +0000 %Y")))			
	photo = urllib2.urlopen((t[0]))
	telebot.sendPhoto(chat_id=chat_id, photo =('club1+club2.png',photo), caption=u"Состав на игру")
	locale.setlocale(locale.LC_TIME, 'ru_RU.utf8')


def time_to_game():
	club1, club2, game_time = parseMatches()
	match_start = datetime.datetime.fromtimestamp(game_time)
	now = datetime.datetime.now()

	if (club1 == u'Зенит'): 
		opposition = club2
	else:
		opposition = club1

	t = match_start - now
	
	if (t.days > 0):
		t = u"%s, %s, %s до начала матча с командой %s" % (sklon(t.days, ar_days ), sklon(t.seconds/3600, ar_hours), sklon( t.seconds%3600/60, ar_minutes), opposition)
	elif (t.seconds/3600 > 0) :
		t = u"%s %s до начала матча" % (sklon(t.seconds/3600, ar_hours), sklon( t.seconds%3600/60, ar_minutes))
	else:
		t = u"Начинается через %s" % (sklon( t.seconds%3600/60, ar_minutes))

	return t

def notify(**kwargs):
	level = '0'
	global telebot
	while True:
		club1, club2, game_time = parseMatches()
		match_start = datetime.datetime.fromtimestamp(game_time)
		twenty_four_hours = datetime.timedelta(hours=24)
		one_hour = datetime.timedelta(hours=1)
		ten_minutes  = datetime.timedelta(minutes=10)
		now = datetime.datetime.now()
		if kwargs['debug']:
			logging.debug('Debug mode started')
			now = set_now(match_start,level)
			notify_at = now 
		elif match_start - now > twenty_four_hours:
			level = '2'
			notify_at = match_start - twenty_four_hours
			resu = club1 + " : " + club2 + u" начнется через сутки"
		elif match_start - now < twenty_four_hours and match_start - now > one_hour:
			level = '3'
			resu = club1 + " : " + club2 + u" начнется ровно через час"
			notify_at = match_start - one_hour
		elif match_start - now < one_hour:
			level = '4'
			notify_at = notify_at + ten_minutes
			try:
				locale.setlocale(locale.LC_TIME, 'en_US.utf8')
				t = get_twits.get_twit(club1=club1,club2=club2)
				twit_time = datetime.datetime.fromtimestamp(time.mktime(time.strptime(t[1],"%a %b %d %H:%M:%S +0000 %Y")))
				if  twit_time > match_start - twenty_four_hours:
					photo = urllib2.urlopen((t[0]))
					telebot.sendPhoto(chat_id=chat_id, photo =('club1+club2.png',photo), caption=u"Состав на игру")
					notify_at = notify_at + one_hour*2
				locale.setlocale(locale.LC_TIME, 'ru_RU.utf8')
			except Exception, e:
				print "Couldn't do it: %s" % e
				pass
			resu = ''
		else: 
			level = '5'
			notify_at = match_start + one_hour*2

		wait_time = notify_at - now 

		time.sleep(wait_time.total_seconds())
		if resu:
			telebot.sendMessage(chat_id=chat_id, text=resu, parse_mode='HTML', disable_web_page_preview='False', disable_notification='False')    

def handler(msg):
	content_type, chat_type, chat_id = telepot.glance(msg)
	print(content_type, chat_type, chat_id)

	command = msg['text'].strip().lower()
	# to have those show up automaticaly in message suggestions configure bot via BotFather 
	if command == '/when':
		message = time_to_game()
	if command == '/lineup':
		message = "send_lineup()"
	if command == '/why':
		message = why_god_why()
	if content_type == 'text':
		telebot.sendMessage(chat_id, message)


def main():
	t1 = threading.Thread(target=notify, kwargs={'debug':None})
	t1.daemon = True
	t1.start()
	telebot.message_loop(handler, run_forever='Listening ...')


if __name__ == "__main__":
    main()	