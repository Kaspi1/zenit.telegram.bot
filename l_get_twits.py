# -*- coding: utf-8 -*- 
from twitter import *
import datetime
import ConfigParser


configParser = ConfigParser.RawConfigParser()
configFilePath = r'config.ini'
configParser.read(configFilePath)

ConsumerKey = configParser.get('twitter', 'ConsumerKey')
ConsumerSecret = configParser.get('twitter', 'ConsumerSecret')
AccessToken = configParser.get('twitter', 'AccessToken')
AccessTokenSecret = configParser.get('twitter', 'AccessTokenSecret')



twitter = Twitter(
    auth=OAuth(AccessToken, AccessTokenSecret, ConsumerKey, ConsumerSecret))


def get_twit(**kwargs):
	club1 = kwargs['club1'] 
	club2 = kwargs['club2'] 
	hashtag = "#{}{}".format(club1, club2)
	query = "состав {} from:zenit_spb filter:twimg".format(hashtag)
	t = twitter.search.tweets(q="состав from:zenit_spb filter:twimg")


	return (t['statuses'][0]['entities']["media"][0]["media_url"],t['statuses'][0]['created_at'])

def main():
	get_twit()

if __name__ == "__main__":
    main()
